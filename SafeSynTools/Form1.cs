﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
using System.IO;
using System.Transactions;

namespace SafeSynTools
{
    public partial class Form1 : Form
    {
        string tableName_seconds = ConfigurationManager.AppSettings["tableName_seconds"].ToString();//获取实时同步的表名
        int seconds_time = Convert.ToInt32(ConfigurationManager.AppSettings["seconds_time"]);//获取实时同步的执行间隔时间
        string mainConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_main"].ToString();//main
        string reportConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_report"].ToString();//report
        string reportAConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_reportA"].ToString();//report2
        string reportBConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_reportB"].ToString();//report2
        string loaclConnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_local"].ToString();//local
        string sql_totalIn = ConfigurationManager.ConnectionStrings["sqlConnectionString_totalin"].ToString();
        string sql_totalInA = ConfigurationManager.ConnectionStrings["sqlConnectionString_totalinA"].ToString();
        string sql_totalInB = ConfigurationManager.ConnectionStrings["sqlConnectionString_totalinB"].ToString();
        string sql_totalOrder = ConfigurationManager.ConnectionStrings["sqlConnectionString_totalorder"].ToString();
        string sql_totalOrderA = ConfigurationManager.ConnectionStrings["sqlConnectionString_totalorderA"].ToString();
        string sql_totalOrderB = ConfigurationManager.ConnectionStrings["sqlConnectionString_totalorderB"].ToString();
        string errorLogPath = System.Environment.CurrentDirectory + "\\errorlog.txt";//错误日志记录路径
        string successLogPath = System.Environment.CurrentDirectory + "\\successlog.txt";//成功日志记录路径
        System.Timers.Timer compareDelayTimer = new System.Timers.Timer();
        bool reportDelay = false;

        public Form1()
        {
            InitializeComponent();
            seconds_time = seconds_time * 1000;//秒
            Label[] labelarr = new Label[] { label1, label2, label3 };
            string[] tablename_seconds = tableName_seconds.Split(',');
            for (int i = 0; i < tablename_seconds.Length; i++)
            {
                if (i % 3 == 0)
                {
                    labelarr[0].Text = labelarr[0].Text + " " + tablename_seconds[i];
                }
                else if (i % 3 == 1)
                {
                    labelarr[1].Text = labelarr[1].Text + " " + tablename_seconds[i];
                }
                else
                {
                    labelarr[2].Text = labelarr[2].Text + " " + tablename_seconds[i];
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            FillMsg1("正在同步...");
            Thread thread = new Thread(new ThreadStart(Run_seconds));
            thread.Start();

            compareDelayTimer.Interval = 10000;
            compareDelayTimer.AutoReset = false;
            compareDelayTimer.Elapsed += new System.Timers.ElapsedEventHandler(compareSQLTime_Elapsed);
            compareDelayTimer.Start();
        }

        #region 实时同步的方法
        void Run_seconds()
        {
            if (string.IsNullOrEmpty(tableName_seconds) == false)
            {
                while (true)
                {
                    if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
                    {
                        Thread.Sleep(1000 * 60 * 60 * 3);
                        continue;
                    }
                    try
                    {
                        string[] tablename_seconds = tableName_seconds.Split(',');
                        foreach (var tablename in tablename_seconds)
                        {
                            try
                            {
                                string field = "";
                                if (tablename == "dt_users")
                                {
                                    field = "id,identityid,group_id,agent_id,is_agent,nick_name,user_name,flog,reg_time,lockid";
                                }
                                else if (tablename == "dt_users_class")
                                {
                                    field = "id,identityid,father_id,user_id,user_class,isagent,uclass,addtime,lockid ";
                                }
                                else if (tablename == "dt_diction_quickpay")
                                {
                                    field = "id,PayName,Fastpay,Alipay,Wechat,QQ,Unionpay,PayUrl,AddTime,lockid";
                                }
                                else if (tablename == "dt_tenant")
                                {
                                    field = "id,identityid,SiteName,AdminName,AdminPwd,MemberName,TestName,Salt,AccountHolder,MobileNumber,Remark,SiteTitle,AddTime,VerFlog,StyleId,StyleUrl,NavCode,lockid,DBtype";
                                }
                                else if (tablename == "dt_users_register")
                                {
                                    field = "id,identityid,UserId,UserName,SourceName,InviteId,InviteUrl,Tester,AddTime,lockid";
                                }
                                else if (tablename == "dt_user_newpay" || tablename == "dt_user_newpay2" || tablename == "dt_user_newpay3")
                                {
                                    field = "id,identityid,userid,username,money,commt,sourcename,addtime,agent_id,inaccountid,group_id,flog,regtime,agentname,lockid";
                                }

                                List<SqlParameter> LocalSqlParamter = new List<SqlParameter>()
                                {
                                    new SqlParameter("@tableName",tablename)
                                };
                                string querystr = "select lockid from dt_tablelockid where tableName=@tableName";
                                var locktable = SqlDbHelper.GetQuery(querystr, LocalSqlParamter.ToArray(), loaclConnStr);
                                if (locktable.Rows.Count > 0)
                                {
                                    byte[] lockid = StringConvertByte(locktable.Rows[0]["lockid"].ToString());
                                    List<SqlParameter> AzureSqlParamter = new List<SqlParameter>();
                                    SqlParameter pa_lockid = new SqlParameter("@lockid", SqlDbType.Timestamp);
                                    pa_lockid.Value = lockid;
                                    AzureSqlParamter.Add(pa_lockid);
                                    string str = "";
                                    if (tablename == "dt_users_class")
                                        str = "select top 1000 " + field + " from " + tablename + " where reportSyn=0 and identityid in (select identityid from dt_tenant where dbtype in (1,2,3)) order by lockid asc";
                                    else if (tablename == "dt_user_newpay" || tablename == "dt_user_newpay2" || tablename == "dt_user_newpay3")
                                        str = "select top 1000 " + field + " from dt_user_newpay where reportSyn=0 and money>0 order by id asc";
                                    else
                                        str = "select top 1000 " + field + " from " + tablename + " where lockid>@lockid order by lockid asc";
                                    var table = SqlDbHelper.GetQuery(str, AzureSqlParamter.ToArray(), (tablename == "dt_user_newpay3" ? reportBConnStr : (tablename == "dt_user_newpay2" ? reportAConnStr : (reportDelay == true ? mainConnStr : reportConnStr))));
                                    //var table = SqlDbHelper.GetQuery(str, AzureSqlParamter.ToArray(), (tablename == "dt_user_newpay2" ? report2ConnStr : (DateTime.Now.Hour >= 0 && DateTime.Now.Hour < 9 && reportDelay == true ? mainConnStr : reportConnStr)));
                                    //var table = SqlDbHelper.GetQuery(str, AzureSqlParamter.ToArray(), (tablename == "dt_user_newpay2" ? report2ConnStr : reportConnStr));
                                    if (table == null)
                                        continue;
                                    if (table.Rows.Count != 0)//有数据时更新到本地数据库中
                                    {
                                        List<string> list_id = new List<string>();
                                        List<string> list_userid = new List<string>();

                                        //将最大lockid转换成字符串
                                        byte[] a = (byte[])table.Rows[table.Rows.Count - 1]["lockid"];
                                        string maxlockid = "0x" + BitConverter.ToString(a).Replace("-", "");

                                        string idStr = "";
                                        //檢查本地有沒有一樣的id
                                        for (int r = 0; r < table.Rows.Count; r++)
                                        {
                                            idStr += table.Rows[r]["id"].ToString().TrimEnd() + ",";
                                            if (tablename == "dt_users_class")
                                            {
                                                list_id.Add(table.Rows[r]["id"].ToString().TrimEnd());
                                                list_userid.Add(table.Rows[r]["user_id"].ToString().TrimEnd());
                                            }
                                            else if(tablename == "dt_user_newpay" || tablename == "dt_user_newpay2" || tablename == "dt_user_newpay3")
                                                list_id.Add(table.Rows[r]["id"].ToString().TrimEnd());
                                        }
                                        //idStr = idStr.Substring(0, idStr.Length - 1);
                                        //string str_local = string.Format("select id from {0} where id in (" + idStr + ")", tablename);
                                        //DataTable dt_select = SqlDbHelper.GetQuery(str_local, loaclConnStr);
                                        //idStr = "";
                                        //for (int r = 0; r < dt_select.Rows.Count; r++)
                                        //{
                                        //    idStr += dt_select.Rows[r]["id"].ToString().TrimEnd() + ",";
                                        //}
                                        //执行删除语句
                                        //if (idStr != "")
                                        //{
                                        idStr = idStr.Substring(0, idStr.Length - 1);
                                        string sql = string.Format("delete from {0} where id in (" + idStr + ")", tablename);
                                        SqlDbHelper.ExecuteNonQuery(sql, loaclConnStr);
                                        if (tablename == "dt_users_class")
                                        {
                                            SqlDbHelper.ExecuteNonQuery(sql, sql_totalOrder);
                                            SqlDbHelper.ExecuteNonQuery(sql, sql_totalOrderA);
                                            SqlDbHelper.ExecuteNonQuery(sql, sql_totalOrderB);
                                        }
                                        else if (tablename == "dt_diction_quickpay")
                                        {
                                            SqlDbHelper.ExecuteNonQuery(sql, sql_totalIn);
                                            SqlDbHelper.ExecuteNonQuery(sql, sql_totalInA);
                                            SqlDbHelper.ExecuteNonQuery(sql, sql_totalInB);
                                        }
                                        //}
                                        bool successShare = true;
                                        bool successTotal = true;
                                        bool successTotalA = true;
                                        bool successTotalB = true;
                                        //执行批量插入操作
                                        RunSqlBulkCopy(table, tablename, ref successShare, loaclConnStr);
                                        if (tablename == "dt_users_class")
                                        {
                                            RunSqlBulkCopy(table, tablename, ref successTotal, sql_totalOrder);
                                            RunSqlBulkCopy(table, tablename, ref successTotalA, sql_totalOrderA);
                                            RunSqlBulkCopy(table, tablename, ref successTotalB, sql_totalOrderB);
                                        }
                                        else if (tablename == "dt_diction_quickpay")
                                        {
                                            RunSqlBulkCopy(table, tablename, ref successTotal, sql_totalIn);
                                            RunSqlBulkCopy(table, tablename, ref successTotalA, sql_totalInA);
                                            RunSqlBulkCopy(table, tablename, ref successTotalB, sql_totalInB);
                                        }

                                        if (successShare == true && successTotal == true && successTotalA == true && successTotalB == true)
                                        {
                                            if (tablename == "dt_users_class" || tablename == "dt_user_newpay" || tablename == "dt_user_newpay2" || tablename == "dt_user_newpay3")
                                            {
                                                for(int i=0;i< list_id.Count; i++)
                                                {
                                                    if (tablename == "dt_users_class")
                                                    {
                                                        List<SqlParameter> SqlParamter = new List<SqlParameter>()
                                                        {
                                                            new SqlParameter("@user_id",list_userid[i]),
                                                            new SqlParameter("@id",list_id[i])
                                                        };
                                                        SqlDbHelper.ExecuteNonQuery("update dt_users_class set reportSyn=2 from dt_users_class with(index(index_userid)) where user_id = @user_id and id=@id", SqlParamter.ToArray(), mainConnStr);
                                                    }
                                                    else
                                                    {
                                                        List<SqlParameter> SqlParamter = new List<SqlParameter>()
                                                        {
                                                            new SqlParameter("@id",list_id[i])
                                                        };
                                                        SqlDbHelper.ExecuteNonQuery("update dt_user_newpay set reportSyn=2 where id=@id", SqlParamter.ToArray(), (tablename == "dt_user_newpay3" ? reportBConnStr : (tablename == "dt_user_newpay2" ? reportAConnStr : mainConnStr)));

                                                    }
                                                }
                                            }
                                            //更新本地list中对应表的lockid
                                            updateDt_tablelockIdByKey(tablename, maxlockid, loaclConnStr);
                                            if (tablename == "dt_users" || tablename == "dt_tenant" || tablename == "dt_user_newpay2")
                                            {
                                                FillMsg1(tablename + "表成功同步" + table.Rows.Count + "条数据...");
                                            }
                                            else if (tablename == "dt_users_class" || tablename == "dt_users_register" || tablename == "dt_user_newpay3")
                                            {
                                                FillMsg2(tablename + "表成功同步" + table.Rows.Count + "条数据...");
                                            }
                                            else
                                            {
                                                FillMsg3(tablename + "表成功同步" + table.Rows.Count + "条数据...");
                                            }
                                        }
                                        else
                                        {
                                            FillErrorMsg(tablename + "同步失敗");
                                            WriteErrorLog(tablename + "同步失敗" + DateTime.Now.ToString(), "");
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message != "未将对象引用设置到对象的实例。")
                                {
                                    FillErrorMsg(tablename + ":" + ex);
                                    WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                                }
                            }
                        }
                    }
                    catch
                    {
                    }
                    finally
                    {
                        Thread.Sleep(seconds_time);//睡眠时间
                    }
                }
            }
        }

        private byte[] StringConvertByte(string sqlstring)
        {
            string stringFromSQL = sqlstring;
            List<byte> byteList = new List<byte>();

            string hexPart = stringFromSQL.Substring(2);
            for (int i = 0; i < hexPart.Length / 2; i++)
            {
                string hexNumber = hexPart.Substring(i * 2, 2);
                byteList.Add((byte)Convert.ToInt32(hexNumber, 16));
            }
            byte[] original = byteList.ToArray();
            return original;
        }

        private void RunSqlBulkCopy(DataTable dt, string tablename, ref bool isSuccess, string connectString)
        {
            if (dt.Rows.Count != 0)
            {
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connectString))
                {
                    sqlBulkCopy.DestinationTableName = tablename;
                    sqlBulkCopy.BulkCopyTimeout = 6000;
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (dt.Columns[i].ColumnName != "lockid")
                            sqlBulkCopy.ColumnMappings.Add(dt.Columns[i].ColumnName, dt.Columns[i].ColumnName);
                    }
                    try
                    {
                        isSuccess = true;
                        sqlBulkCopy.WriteToServer(dt);
                    }
                    catch (Exception ex)
                    {
                        isSuccess = false;
                        FillErrorMsg("批量插入" + tablename + ":" + ex);
                        WriteErrorLog("批量插入" + tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                    }
                }
            }
        }

        void updateDt_tablelockIdByKey(string tableName, string lockid, string connectString)
        {
            string sql = string.Format("update dt_tablelockid set lockid='{0}',add_time='{1}' where tableName='{2}'", lockid, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), tableName);
            SqlDbHelper.ExecuteNonQuery(sql, connectString);
        }
        #endregion

        #region richTextBox记录
        private delegate void RichBox1(string msg);
        private void FillMsg1(string msg)
        {
            if (richTextBox1.InvokeRequired)
            {
                RichBox1 rb = new RichBox1(FillMsg1);
                richTextBox1.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.AppendText(msg);
                    richTextBox1.AppendText("\r\n");
                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.SelectionLength = 0;
                    richTextBox1.Focus();
                }
            }
        }

        private delegate void RichBox2(string msg);
        private void FillMsg2(string msg)
        {
            if (richTextBox2.InvokeRequired)
            {
                RichBox2 rb = new RichBox2(FillMsg2);
                richTextBox2.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox2.IsHandleCreated)
                {
                    richTextBox2.AppendText(msg);
                    richTextBox2.AppendText("\r\n");
                    richTextBox2.SelectionStart = richTextBox2.Text.Length;
                    richTextBox2.SelectionLength = 0;
                    richTextBox2.Focus();
                }
            }
        }

        private delegate void RichBox3(string msg);
        private void FillMsg3(string msg)
        {
            if (richTextBox3.InvokeRequired)
            {
                RichBox3 rb = new RichBox3(FillMsg3);
                richTextBox3.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox3.IsHandleCreated)
                {
                    richTextBox3.AppendText(msg);
                    richTextBox3.AppendText("\r\n");
                    richTextBox3.SelectionStart = richTextBox3.Text.Length;
                    richTextBox3.SelectionLength = 0;
                    richTextBox3.Focus();
                }
            }
        }

        private delegate void RichBoxErr(string msg);
        private void FillErrorMsg(string msg)
        {
            if (errorBox.InvokeRequired)
            {
                RichBoxErr rb = new RichBoxErr(FillErrorMsg);
                errorBox.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (errorBox.IsHandleCreated)
                {
                    errorBox.AppendText(msg);
                    errorBox.AppendText("\t\n");
                    errorBox.SelectionStart = errorBox.Text.Length;
                    errorBox.SelectionLength = 0;
                    errorBox.Focus();
                }
            }
        }
        #endregion

        #region 打印成功日志记录
        private object obj1 = new object();
        public void WriteLogData(string msgex)
        {
            lock (obj1)
            {
                if (!File.Exists(successLogPath))
                {
                    FileStream fs1 = new FileStream(successLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.Write(msgex);
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(successLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.Write(msgex);
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 打印错误日志记录
        private object obj = new object();
        public void WriteErrorLog(string msgex, string msgsql)
        {
            lock (obj)
            {
                if (!File.Exists(errorLogPath))
                {
                    FileStream fs1 = new FileStream(errorLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.WriteLine(msgex);
                    sw.WriteLine(msgsql);
                    sw.WriteLine();
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(errorLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.WriteLine(msgex);
                    sr.WriteLine(msgsql);
                    sr.WriteLine();
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Dispose();
            Application.Exit();
            System.Environment.Exit(0);
        }

        private void compareSQLTime_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                string cmd = "select MAX(add_time) as add_time from dt_lottery_orders";
                DataTable tempTable = new DataTable();
                DateTime timeMain;
                DateTime timeReport;

                tempTable = SqlDbHelper.GetQuery(cmd, mainConnStr);
                timeMain = Convert.ToDateTime(tempTable.Rows[0]["add_time"]);
                tempTable = SqlDbHelper.GetQuery(cmd, reportConnStr);
                timeReport = Convert.ToDateTime(tempTable.Rows[0]["add_time"]);

                if (timeMain.Subtract(timeReport).TotalSeconds >= 60)
                {
                    //delaySecond = Convert.ToInt32(timeMain.Subtract(timeReport).TotalSeconds);
                    if (reportDelay == false)
                    {
                        reportDelay = true;
                        WriteErrorLog("--------------------------\r\n報表從庫延遲", DateTime.Now.ToString() + "\r\n--------------------------");
                    }
                }
                else
                {
                    //delaySecond = 0;
                    if (reportDelay == true)
                    {
                        WriteErrorLog("--------------------------\r\n報表從庫恢復", DateTime.Now.ToString() + "\r\n--------------------------");
                    }
                    reportDelay = false;
                }
            }
            catch (Exception ex)
            {
                reportDelay = true;
            }
            finally
            {
                compareDelayTimer.Start();
            }
        }
    }
}
